import 'package:countdown/common/routers/routes.dart';
import 'package:countdown/common/store/store.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

/// 第一次欢迎页面
class RouteWelcomeMiddleware extends GetMiddleware {
  // priority 数字小优先级高
  @override
  int? priority = 0;

  RouteWelcomeMiddleware({required this.priority});

  @override
  RouteSettings? redirect(String? route) {
    if (ConfigStore.to.isFirstOpen == true) {
      //已同意不做处理
      return null;
    }
    return const RouteSettings(name: AppRoutes.application);
    // if (UserStore.to.isLogin == true) {
    //   return RouteSettings(name: AppRoutes.application);
    // }
    // if (UserStore.to.isLogin == false) {
    //   return RouteSettings(name: AppRoutes.SIGN_IN);
    // }
  }
}
