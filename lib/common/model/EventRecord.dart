import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';

part 'EventRecord.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的 运行flutter pub run build_runner build
@collection
class EventRecord {
  Id? id = Isar.autoIncrement; // 你也可以用 id = null 来表示 id 是自增的
  ///事件类型
  int? type;
  String? emoji;

  //事件名称构建索引
  @Index(type: IndexType.value)
  String? title;

  DateTime? date;

  //是否存档
  bool? ifArchive;
  //是否置顶
  bool? ifTop;
  //排序字段
  @Index()
  int? sort;

  int dayBetween;
  //卡片标题
  String? cardTitle;
  //卡片副标题
  String? cardSubTitle;
  //是否农历日期
  bool? lunar;

  EventRecord({
    this.id,
    this.type,
    this.emoji,
    this.title,
    this.date,
    this.ifArchive,
    this.sort,
    this.dayBetween = 0,
  });

  factory EventRecord.fromJson(Map<String, dynamic> json) => EventRecord(
        id: json["id"],
        type: json["type"],
        emoji: json["emoji"],
        title: json["title"],
        date: json["date"],
        ifArchive: json["ifArchive"],
        sort: json["sort"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "emoji": emoji,
        "title": title,
        "date": date,
        "ifArchive": ifArchive,
        "sort": sort,
      };
}
