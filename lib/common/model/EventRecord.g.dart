// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'EventRecord.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters

extension GetEventRecordCollection on Isar {
  IsarCollection<EventRecord> get eventRecords => this.collection();
}

const EventRecordSchema = CollectionSchema(
  name: r'EventRecord',
  id: -7634061142836849820,
  properties: {
    r'cardSubTitle': PropertySchema(
      id: 0,
      name: r'cardSubTitle',
      type: IsarType.string,
    ),
    r'cardTitle': PropertySchema(
      id: 1,
      name: r'cardTitle',
      type: IsarType.string,
    ),
    r'date': PropertySchema(
      id: 2,
      name: r'date',
      type: IsarType.dateTime,
    ),
    r'dayBetween': PropertySchema(
      id: 3,
      name: r'dayBetween',
      type: IsarType.long,
    ),
    r'emoji': PropertySchema(
      id: 4,
      name: r'emoji',
      type: IsarType.string,
    ),
    r'ifArchive': PropertySchema(
      id: 5,
      name: r'ifArchive',
      type: IsarType.bool,
    ),
    r'ifTop': PropertySchema(
      id: 6,
      name: r'ifTop',
      type: IsarType.bool,
    ),
    r'lunar': PropertySchema(
      id: 7,
      name: r'lunar',
      type: IsarType.bool,
    ),
    r'sort': PropertySchema(
      id: 8,
      name: r'sort',
      type: IsarType.long,
    ),
    r'title': PropertySchema(
      id: 9,
      name: r'title',
      type: IsarType.string,
    ),
    r'type': PropertySchema(
      id: 10,
      name: r'type',
      type: IsarType.long,
    )
  },
  estimateSize: _eventRecordEstimateSize,
  serialize: _eventRecordSerialize,
  deserialize: _eventRecordDeserialize,
  deserializeProp: _eventRecordDeserializeProp,
  idName: r'id',
  indexes: {
    r'title': IndexSchema(
      id: -7636685945352118059,
      name: r'title',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'title',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    ),
    r'sort': IndexSchema(
      id: 5566940169709045701,
      name: r'sort',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'sort',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _eventRecordGetId,
  getLinks: _eventRecordGetLinks,
  attach: _eventRecordAttach,
  version: '3.0.5',
);

int _eventRecordEstimateSize(
  EventRecord object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.cardSubTitle;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.cardTitle;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.emoji;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.title;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _eventRecordSerialize(
  EventRecord object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.cardSubTitle);
  writer.writeString(offsets[1], object.cardTitle);
  writer.writeDateTime(offsets[2], object.date);
  writer.writeLong(offsets[3], object.dayBetween);
  writer.writeString(offsets[4], object.emoji);
  writer.writeBool(offsets[5], object.ifArchive);
  writer.writeBool(offsets[6], object.ifTop);
  writer.writeBool(offsets[7], object.lunar);
  writer.writeLong(offsets[8], object.sort);
  writer.writeString(offsets[9], object.title);
  writer.writeLong(offsets[10], object.type);
}

EventRecord _eventRecordDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = EventRecord(
    date: reader.readDateTimeOrNull(offsets[2]),
    dayBetween: reader.readLongOrNull(offsets[3]) ?? 0,
    emoji: reader.readStringOrNull(offsets[4]),
    id: id,
    ifArchive: reader.readBoolOrNull(offsets[5]),
    sort: reader.readLongOrNull(offsets[8]),
    title: reader.readStringOrNull(offsets[9]),
    type: reader.readLongOrNull(offsets[10]),
  );
  object.cardSubTitle = reader.readStringOrNull(offsets[0]);
  object.cardTitle = reader.readStringOrNull(offsets[1]);
  object.ifTop = reader.readBoolOrNull(offsets[6]);
  object.lunar = reader.readBoolOrNull(offsets[7]);
  return object;
}

P _eventRecordDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 3:
      return (reader.readLongOrNull(offset) ?? 0) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (reader.readBoolOrNull(offset)) as P;
    case 6:
      return (reader.readBoolOrNull(offset)) as P;
    case 7:
      return (reader.readBoolOrNull(offset)) as P;
    case 8:
      return (reader.readLongOrNull(offset)) as P;
    case 9:
      return (reader.readStringOrNull(offset)) as P;
    case 10:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _eventRecordGetId(EventRecord object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _eventRecordGetLinks(EventRecord object) {
  return [];
}

void _eventRecordAttach(
    IsarCollection<dynamic> col, Id id, EventRecord object) {
  object.id = id;
}

extension EventRecordQueryWhereSort
    on QueryBuilder<EventRecord, EventRecord, QWhere> {
  QueryBuilder<EventRecord, EventRecord, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhere> anyTitle() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'title'),
      );
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhere> anySort() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'sort'),
      );
    });
  }
}

extension EventRecordQueryWhere
    on QueryBuilder<EventRecord, EventRecord, QWhereClause> {
  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> idGreaterThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> titleIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'title',
        value: [null],
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> titleIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'title',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> titleEqualTo(
      String? title) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'title',
        value: [title],
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> titleNotEqualTo(
      String? title) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'title',
              lower: [],
              upper: [title],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'title',
              lower: [title],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'title',
              lower: [title],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'title',
              lower: [],
              upper: [title],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> titleGreaterThan(
    String? title, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'title',
        lower: [title],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> titleLessThan(
    String? title, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'title',
        lower: [],
        upper: [title],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> titleBetween(
    String? lowerTitle,
    String? upperTitle, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'title',
        lower: [lowerTitle],
        includeLower: includeLower,
        upper: [upperTitle],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> titleStartsWith(
      String TitlePrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'title',
        lower: [TitlePrefix],
        upper: ['$TitlePrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> titleIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'title',
        value: [''],
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> titleIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'title',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'title',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'title',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'title',
              upper: [''],
            ));
      }
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> sortIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'sort',
        value: [null],
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> sortIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'sort',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> sortEqualTo(
      int? sort) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'sort',
        value: [sort],
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> sortNotEqualTo(
      int? sort) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'sort',
              lower: [],
              upper: [sort],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'sort',
              lower: [sort],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'sort',
              lower: [sort],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'sort',
              lower: [],
              upper: [sort],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> sortGreaterThan(
    int? sort, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'sort',
        lower: [sort],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> sortLessThan(
    int? sort, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'sort',
        lower: [],
        upper: [sort],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterWhereClause> sortBetween(
    int? lowerSort,
    int? upperSort, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'sort',
        lower: [lowerSort],
        includeLower: includeLower,
        upper: [upperSort],
        includeUpper: includeUpper,
      ));
    });
  }
}

extension EventRecordQueryFilter
    on QueryBuilder<EventRecord, EventRecord, QFilterCondition> {
  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'cardSubTitle',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'cardSubTitle',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'cardSubTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'cardSubTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'cardSubTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'cardSubTitle',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'cardSubTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'cardSubTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'cardSubTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'cardSubTitle',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'cardSubTitle',
        value: '',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardSubTitleIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'cardSubTitle',
        value: '',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'cardTitle',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'cardTitle',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'cardTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'cardTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'cardTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'cardTitle',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'cardTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'cardTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'cardTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'cardTitle',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'cardTitle',
        value: '',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      cardTitleIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'cardTitle',
        value: '',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> dateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'date',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      dateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'date',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> dateEqualTo(
      DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'date',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> dateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'date',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> dateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'date',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> dateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'date',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      dayBetweenEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dayBetween',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      dayBetweenGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dayBetween',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      dayBetweenLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dayBetween',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      dayBetweenBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dayBetween',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> emojiIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'emoji',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      emojiIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'emoji',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> emojiEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'emoji',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      emojiGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'emoji',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> emojiLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'emoji',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> emojiBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'emoji',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> emojiStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'emoji',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> emojiEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'emoji',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> emojiContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'emoji',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> emojiMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'emoji',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> emojiIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'emoji',
        value: '',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      emojiIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'emoji',
        value: '',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> idEqualTo(
      Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      ifArchiveIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'ifArchive',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      ifArchiveIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'ifArchive',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      ifArchiveEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'ifArchive',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> ifTopIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'ifTop',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      ifTopIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'ifTop',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> ifTopEqualTo(
      bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'ifTop',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> lunarIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'lunar',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      lunarIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'lunar',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> lunarEqualTo(
      bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'lunar',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> sortIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'sort',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      sortIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'sort',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> sortEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'sort',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> sortGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'sort',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> sortLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'sort',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> sortBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'sort',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> titleIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'title',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      titleIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'title',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> titleEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'title',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      titleGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'title',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> titleLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'title',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> titleBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'title',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> titleStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'title',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> titleEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'title',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> titleContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'title',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> titleMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'title',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> titleIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'title',
        value: '',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      titleIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'title',
        value: '',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> typeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'type',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition>
      typeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'type',
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> typeEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'type',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> typeGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'type',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> typeLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'type',
        value: value,
      ));
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterFilterCondition> typeBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'type',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension EventRecordQueryObject
    on QueryBuilder<EventRecord, EventRecord, QFilterCondition> {}

extension EventRecordQueryLinks
    on QueryBuilder<EventRecord, EventRecord, QFilterCondition> {}

extension EventRecordQuerySortBy
    on QueryBuilder<EventRecord, EventRecord, QSortBy> {
  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByCardSubTitle() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'cardSubTitle', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy>
      sortByCardSubTitleDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'cardSubTitle', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByCardTitle() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'cardTitle', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByCardTitleDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'cardTitle', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'date', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'date', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByDayBetween() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dayBetween', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByDayBetweenDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dayBetween', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByEmoji() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'emoji', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByEmojiDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'emoji', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByIfArchive() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ifArchive', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByIfArchiveDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ifArchive', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByIfTop() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ifTop', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByIfTopDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ifTop', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByLunar() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lunar', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByLunarDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lunar', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortBySort() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sort', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortBySortDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sort', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByTitle() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'title', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByTitleDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'title', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'type', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> sortByTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'type', Sort.desc);
    });
  }
}

extension EventRecordQuerySortThenBy
    on QueryBuilder<EventRecord, EventRecord, QSortThenBy> {
  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByCardSubTitle() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'cardSubTitle', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy>
      thenByCardSubTitleDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'cardSubTitle', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByCardTitle() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'cardTitle', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByCardTitleDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'cardTitle', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'date', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'date', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByDayBetween() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dayBetween', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByDayBetweenDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dayBetween', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByEmoji() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'emoji', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByEmojiDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'emoji', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByIfArchive() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ifArchive', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByIfArchiveDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ifArchive', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByIfTop() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ifTop', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByIfTopDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ifTop', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByLunar() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lunar', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByLunarDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lunar', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenBySort() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sort', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenBySortDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sort', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByTitle() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'title', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByTitleDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'title', Sort.desc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'type', Sort.asc);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QAfterSortBy> thenByTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'type', Sort.desc);
    });
  }
}

extension EventRecordQueryWhereDistinct
    on QueryBuilder<EventRecord, EventRecord, QDistinct> {
  QueryBuilder<EventRecord, EventRecord, QDistinct> distinctByCardSubTitle(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'cardSubTitle', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QDistinct> distinctByCardTitle(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'cardTitle', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QDistinct> distinctByDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'date');
    });
  }

  QueryBuilder<EventRecord, EventRecord, QDistinct> distinctByDayBetween() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dayBetween');
    });
  }

  QueryBuilder<EventRecord, EventRecord, QDistinct> distinctByEmoji(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'emoji', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QDistinct> distinctByIfArchive() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'ifArchive');
    });
  }

  QueryBuilder<EventRecord, EventRecord, QDistinct> distinctByIfTop() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'ifTop');
    });
  }

  QueryBuilder<EventRecord, EventRecord, QDistinct> distinctByLunar() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'lunar');
    });
  }

  QueryBuilder<EventRecord, EventRecord, QDistinct> distinctBySort() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'sort');
    });
  }

  QueryBuilder<EventRecord, EventRecord, QDistinct> distinctByTitle(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'title', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<EventRecord, EventRecord, QDistinct> distinctByType() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'type');
    });
  }
}

extension EventRecordQueryProperty
    on QueryBuilder<EventRecord, EventRecord, QQueryProperty> {
  QueryBuilder<EventRecord, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<EventRecord, String?, QQueryOperations> cardSubTitleProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'cardSubTitle');
    });
  }

  QueryBuilder<EventRecord, String?, QQueryOperations> cardTitleProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'cardTitle');
    });
  }

  QueryBuilder<EventRecord, DateTime?, QQueryOperations> dateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'date');
    });
  }

  QueryBuilder<EventRecord, int, QQueryOperations> dayBetweenProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dayBetween');
    });
  }

  QueryBuilder<EventRecord, String?, QQueryOperations> emojiProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'emoji');
    });
  }

  QueryBuilder<EventRecord, bool?, QQueryOperations> ifArchiveProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'ifArchive');
    });
  }

  QueryBuilder<EventRecord, bool?, QQueryOperations> ifTopProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'ifTop');
    });
  }

  QueryBuilder<EventRecord, bool?, QQueryOperations> lunarProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'lunar');
    });
  }

  QueryBuilder<EventRecord, int?, QQueryOperations> sortProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'sort');
    });
  }

  QueryBuilder<EventRecord, String?, QQueryOperations> titleProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'title');
    });
  }

  QueryBuilder<EventRecord, int?, QQueryOperations> typeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'type');
    });
  }
}
