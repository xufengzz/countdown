class AppRoutes {
  static const initial = '/';
  static const application = '/application';
  static const list = '/list';
  static const addEvent = '/addEvent';
  static const settings = '/settings';

  static const signIn = '/sign_in';
  static const signUp = '/sign_up';
  static const notFound = '/not_found';
  static const category = '/category';
}
