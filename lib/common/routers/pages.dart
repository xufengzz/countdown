import 'package:countdown/common/middlewares/middlewares.dart';
import 'package:countdown/page/application/index.dart';
import 'package:countdown/page/frame/notfound/index.dart';
import 'package:countdown/page/frame/welcome/index.dart';
import 'package:countdown/page/list/index.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../page/calendar/index.dart';
import '../../page/event/index.dart';
import '../../page/event/view.dart';
import 'routes.dart';

class AppPages {
  static const initial = AppRoutes.initial;
  static final RouteObserver<Route> observer = RouteObservers();
  static List<String> history = [];

  static final List<GetPage> routes = [
    // 免登陆
    GetPage(
      name: AppRoutes.initial,
      page: () => const WelcomePage(),
      binding: WelcomeBinding(),
      transition: Transition.cupertino,
      middlewares: [
        RouteWelcomeMiddleware(priority: 1),
      ],
    ),
    // 导航页
    GetPage(
      name: AppRoutes.application,
      page: () => const ApplicationPage(),
      binding: ApplicationBinding(),
    ),
    //
    GetPage(
      name: AppRoutes.list,
      page: () => const ListPage(),
      binding: ListBinding(),
    ),
    GetPage(
      name: AppRoutes.addEvent,
      page: () => const EventPage(),
      transition: Transition.downToUp,
      binding: EventBinding(),
    ),
    GetPage(
      name: AppRoutes.settings,
      page: () => const SettingsPage(),
    ),
    // GetPage(
    //   name: AppRoutes.SIGN_UP,
    //   page: () => SignUpPage(),
    //   binding: SignUpBinding(),
    // ),
    //
    // // 需要登录
    // GetPage(
    //   name: AppRoutes.Application,
    //   page: () => ApplicationPage(),
    //   binding: ApplicationBinding(),
    //   middlewares: [
    //     RouteAuthMiddleware(priority: 1),
    //   ],
    // ),
    //
    // // 分类列表
    // GetPage(
    //   name: AppRoutes.Category,
    //   page: () => CategoryPage(),
    //   binding: CategoryBinding(),
    // ),
  ];

  static final unknownRoute = GetPage(
    name: AppRoutes.notFound,
    page: () => const NotfoundPage(),
  );
}
