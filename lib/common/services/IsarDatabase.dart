import 'dart:convert';
import 'package:get/get.dart';
import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/EventRecord.dart';

/// 本地存储
class IsarDatabaseService extends GetxService {
  static IsarDatabaseService get to => Get.find();

  late final Isar _isar;

  Future<IsarDatabaseService> init() async {
    final dir = await getApplicationDocumentsDirectory();
    _isar = await Isar.open([EventRecordSchema], directory: dir.path);
    return this;
  }

  Isar isar() {
    return _isar;
  }
}
