import 'package:intl/intl.dart';

class ConstellationUtil {
  ///白羊座 321-419
  ///金牛座 420-520
  ///双子座 521-621
  ///巨蟹座 622-722
  ///狮子座 723-822
  ///处女座 823-922
  ///天秤座 923-1023
  ///天蝎座 1024-1122
  ///射手座 1123-1221
  ///摩羯座 1222-119
  ///水瓶座 120-218
  ///双鱼座 219-320

  static String getConstellationNameFromDateTime(String timeStr) {
    DateTime dateTime = DateFormat('yyyy-MM-dd').parse(timeStr);
    String time = DateFormat("MMdd").format(dateTime);
    print('转化后的时间：$time');
    int timeInt = int.parse(time);
    print("转化后的时间Int：$timeInt");
    if (timeInt >= 321 && timeInt <= 419) {
      return "白羊座";
    }
    if (timeInt >= 420 && timeInt <= 520) {
      return "金牛座";
    }
    if (timeInt >= 521 && timeInt <= 621) {
      return "双子座";
    }
    if (timeInt >= 622 && timeInt <= 722) {
      return "巨蟹座";
    }
    if (timeInt >= 723 && timeInt <= 822) {
      return "狮子座";
    }
    if (timeInt >= 823 && timeInt <= 922) {
      return "处女座";
    }
    if (timeInt >= 923 && timeInt <= 1023) {
      return "天秤座";
    }
    if (timeInt >= 1024 && timeInt <= 1122) {
      return "天蝎座";
    }
    if (timeInt >= 1123 && timeInt <= 1221) {
      return "射手座";
    }
    if (timeInt >= 120 && timeInt <= 218) {
      return "水瓶座";
    }
    if (timeInt >= 219 && timeInt <= 320) {
      return "双鱼座";
    }
    return "摩羯座";
  }
}
