import 'package:intl/intl.dart';
import 'package:date_format/date_format.dart';
import 'package:lunar/calendar/Lunar.dart';
import 'package:lunar/calendar/Solar.dart';
import 'ConstellationUtil.dart';

import 'ConstellationUtil.dart';

/// 格式化时间
String duTimeLineFormat(DateTime dt) {
  var now = DateTime.now();
  var difference = now.difference(dt);

  // 1天内
  if (difference.inHours < 24) {
    return "${difference.inHours} hours ago";
  }
  // 30天内
  else if (difference.inDays < 30) {
    return "${difference.inDays} days ago";
  }
  // MM-dd
  else if (difference.inDays < 365) {
    final dtFormat = DateFormat('MM-dd');
    return dtFormat.format(dt);
  }
  // yyyy-MM-dd
  else {
    final dtFormat = DateFormat('yyyy-MM-dd');
    var str = dtFormat.format(dt);
    return str;
  }
}

var weekday = [" ", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"];

var day = [" ", "周一", "周二", "周三", "周四", "周五", "周六", "周日"];

/// 格式化为标准时间字符串  默认yyyy-mm-dd
String dateTimeFormat(DateTime dt, {DateFormat? dtFormat}) {
  dtFormat = dtFormat ?? DateFormat('yyyy-MM-dd');
  var str = dtFormat.format(dt);
  return str;
}

///格式化记账日期并转换北京时间
String formatKeepingDateTime(DateTime dt) {
  // String utcTime = dt.toString();
  // dt = DateTime.parse("${utcTime.substring(0, 19)}-0800");
  String week = "${weekday[dt.weekday]}";
  String date = formatDate(dt, [yyyy, '/', mm, '/', dd]);
  String text = '上午';
  if (dt.hour < 12) {
    text = '上午';
  } else {
    text = '下午';
  }
  return "$week $text $date";
}

///格式化时间为时间范围段 22/10/01 - 至今
///22/10/01 - 22/10/24
String formatDateTime(
  DateTime startTime,
  DateTime endTime,
) {
  String start = formatDate(startTime, [yyyy, '/', mm, '/', dd]);
  String end = formatDate(endTime, [yyyy, '/', mm, '/', dd]);

  return "$start - $end";
}

///格式化日期  2023年10月8日
String formatDateDay(DateTime dt, {bool lunar = false}) {
  // String week =
  //     ConstellationUtil.getConstellationNameFromDateTime(dt.toString());
  String date = formatDate(dt, [yyyy, '年', mm, '月', dd, '日']);
  if (lunar) {
    //农历
    Lunar lunarDate = Lunar.fromDate(dt);
    return lunarDate.toString();
  }
  return date;

  // return "$date $week ";
}

/// 格式化为标准时间字符串  默认yyyy-mm-dd
String formatDateDay1(DateTime dt, {DateFormat? dtFormat}) {
  String week = day[dt.weekday];
  String date = formatDate(dt, [mm, '月', dd, '日']);

  return "$date $week ";
}

//比较两个日期之间的天数 倒数日处理方法
int daysBetween(DateTime from, DateTime to) {
  from = DateTime(from.year, from.month, from.day);
  to = DateTime(to.year, to.month, to.day);

  return (to.difference(from).inHours / 24).round();
}

//比较两个日期的年份
int yearBetween(DateTime from, DateTime to) {
  from = DateTime(from.year, from.month, from.day);
  to = DateTime(to.year, to.month, to.day);

  return (to.difference(from).inDays / 365).round();
}

//倒计时用格式化成中文
format(int days, {int type = 0}) {
  if (days < 0) {
    return "${(days).abs()}天";
  }

  if (days > 0 && type == 1) {
    return "已过去$days天";
  }
  if (days > 0) {
    return "$days天";
  }
  return "今天";
}

//传入生日 。农历就传农历
formatBirthday(DateTime from, {bool lunar = false}) {
  DateTime today = DateTime.now();
  today = DateTime(today.year, today.month, today.day);
  //用户今年阳历生日 如果过了会计算明年的
  DateTime userToEewYear = from;

  if (!lunar) {
    Solar todaySolarDate = Solar.fromDate(from);
    var solar = Solar.fromYmd(
        today.year, todaySolarDate.getMonth(), todaySolarDate.getDay());
    userToEewYear = solar.getCalendar();
    int days = (userToEewYear.difference(today).inHours / 24).round();
    if (days < 0) {
      //生日已过1天 计算明年的
      var solar = Solar.fromYmd(
          today.year + 1, todaySolarDate.getMonth(), todaySolarDate.getDay());
      userToEewYear = DateFormat("yyyy-MM-dd", 'en').parseStrict(solar.toYmd());
      return (userToEewYear.difference(today).inHours / 24).round();
    }
    return (today.difference(userToEewYear).inHours / 24).round().abs();
  }

  //农历处理
  if (lunar) {
    Lunar todayLunarDate = Lunar.fromDate(today);
    Solar todaySolarDate = Solar.fromDate(today);

    int year = 0;
    if (todayLunarDate.getYear() == todaySolarDate.getYear()) {
      year = todaySolarDate.getYear();
    }
    if (todayLunarDate.getYear() < todaySolarDate.getYear()) {
      year = todayLunarDate.getYear();
    }

    //1.获取用户农历日期
    Lunar lunarDate = Lunar.fromDate(from);
    //查询今年农历转阳历
    var solar = Lunar.fromYmd(year, lunarDate.getMonth(), lunarDate.getDay())
        .getSolar();
    //2.转阳历
    userToEewYear = DateFormat("yyyy-MM-dd", 'en').parseStrict(solar.toYmd());
    int days = (userToEewYear.difference(today).inHours / 24).round();
    if (days < 0) {
      Lunar lunarDate = Lunar.fromDate(from);
      //生日已过1天 计算明年的
      var solar =
          Lunar.fromYmd(year + 1, lunarDate.getMonth(), lunarDate.getDay())
              .getSolar();
      userToEewYear = DateFormat("yyyy-MM-dd", 'en').parseStrict(solar.toYmd());
    }
    return (userToEewYear.difference(today).inHours / 24).round();
  }
}

//根据出生年月计算 X岁或X月X天或X天 1,岁。8个月，20天
String getAgeByBirthday(DateTime birthday) {
  DateTime currentTime = DateTime.now();
  int totalDays = (currentTime.difference(birthday).inHours / 24).round();
  if (totalDays < 365) {
    //个月计算
    int diffmonth = (currentTime.difference(birthday).inDays / 30).round();
    int day = currentTime.day - birthday.day;
    //不足一個月 計算天
    if (diffmonth <= 0) {
      //直接计算天
      return "$totalDays天";
    } else {
      DateTime newbirthday =
          DateTime(birthday.year, birthday.month + diffmonth, birthday.day);
      day = (currentTime.difference(newbirthday).inHours / 24).round();
      return "$diffmonth个月${(day == 0 ? "" : "$day天")}";
    }
  } else {
    //年龄计算
    return "${GetAgeByBirthdate(birthday)}岁";
  }
  return "";
}

//计算多少天
int GetAgeByBirthdate(DateTime birthdate) {
  DateTime now = DateTime.now();
  int age = now.year - birthdate.year;
  int v = age < 0 ? 0 : age;
  if (now.month >= birthdate.month &&
      (now.month != birthdate.month || now.day >= birthdate.day)) {
    return v;
  }
  return v;
}
