import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lottie/lottie.dart';

//loading 猫咪占位
Widget loadingCat() {
  return Center(
    child: Lottie.asset(
        width: 250.w, height: 250.h, 'assets/lottie/lurking-cat.json'),
  );
}

//loading 加载
Widget loading() {
  return Lottie.asset(
      width: 30.w, height: 30.h, 'assets/lottie/loader-cat.json');
}

//猫
Widget loadingBlackCat() {
  return Lottie.asset(
      width: 120.w, height: 120.h, 'assets/lottie/loader-cat.json');
}

//loading 放礼炮动画
Widget loadingSurprise() {
  return Lottie.asset('assets/lottie/loadingSurprise.json');
}
