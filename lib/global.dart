import 'package:bruno/bruno.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'AdsConfig.dart';
import 'common/services/storage.dart';
import 'common/store/config.dart';
import 'common/services/IsarDatabase.dart';
import 'common/utils/loading.dart';

/// 全局静态数据
class Global {
  /// 初始化
  static Future init() async {
    //强制初始化竖屏
    WidgetsFlutterBinding.ensureInitialized();
    await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    setSystemUi();
    Loading();
    //配置ui主题
    BrnInitializer.register(allThemeConfig: BrunoConfigUtils.defaultAllConfig);
    await Get.putAsync<StorageService>(() => StorageService().init());
    await Get.putAsync<IsarDatabaseService>(() => IsarDatabaseService().init());

    Get.put<ConfigStore>(ConfigStore());
    //Get.put<UserStore>(UserStore());
  }

  //android 状态栏为透明的沉浸
  static void setSystemUi() {
    if (GetPlatform.isAndroid) {
      SystemUiOverlayStyle systemUiOverlayStyle = const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark);
      SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    }
  }

  ///主题配置
  static void themeRegister() {
    BrnInitializer.register(
        allThemeConfig: BrnAllThemeConfig(
            // 全局配置
            commonConfig:
                BrnCommonConfig(brandPrimary: const Color(0xFF3072F6)),
            // dialog配置 圆角
            dialogConfig: BrnDialogConfig(radius: 12.0)));
  }
}

class BrunoConfigUtils {
  static BrnAllThemeConfig defaultAllConfig =
      BrnAllThemeConfig(commonConfig: defaultCommonConfig);

  /// 全局配置
  static BrnCommonConfig defaultCommonConfig = BrnCommonConfig(
    ///品牌色
    brandPrimary: const Color(0xFF556efe),

    ///基础文字反色 比如按钮的字体跟主题对应
    //colorTextBaseInverse: const Color(0xFF556efe),
  );
}
