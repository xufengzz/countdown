import 'package:countdown/page/event/index.dart';
import 'package:countdown/page/list/index.dart';
import 'package:get/get.dart';

import 'controller.dart';

class ApplicationBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ApplicationController>(() => ApplicationController());
    //首页列表
    Get.lazyPut<ListController>(() => ListController());
    //事件
    Get.lazyPut<EventController>(() => EventController());
  }
}
