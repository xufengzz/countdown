import 'dart:async';

import 'package:countdown/common/utils/utils.dart';
import 'package:countdown/common/values/values.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:uni_links/uni_links.dart';

import 'index.dart';

class ApplicationController extends GetxController {
  ApplicationController();

  /// 响应式成员变量

  final state = ApplicationState();

  // tab 页标题
  late final List<String> tabTitles;

  // 页控制器
  late final PageController pageController;

  // 底部导航项目
  late final List<BottomNavigationBarItem> bottomTabs;

  // tab栏切换取消动画
  void handleNavBarTap(int index) {
    pageController.jumpToPage(index);
  }

  /// 事件

  // tab栏页码切换
  void handlePageChanged(int page) {
    state.page = page;
  }

  /// scheme 内部打开
  bool isInitialUriIsHandled = false;
  StreamSubscription? uriSub;

  // 第一次打开
  Future<void> handleInitialUri() async {
    if (!isInitialUriIsHandled) {
      isInitialUriIsHandled = true;
      try {
        final uri = await getInitialUri();
        if (uri == null) {
          print('no initial uri');
        } else {
          // 这里获取了 scheme 请求
          print('got initial uri: $uri');
        }
      } on PlatformException {
        print('falied to get initial uri');
      } on FormatException catch (err) {
        print('malformed initial uri, ' + err.toString());
      }
    }
  }

  // 程序打开时介入
  void handleIncomingLinks() {
    if (!kIsWeb) {
      uriSub = uriLinkStream.listen((Uri? uri) {
        // 这里获取了 scheme 请求
        print('got uri: $uri');
        //别人分享url  收到优惠活动数据 路由到优惠页面 赠送vip！
        if (uri != null && uri.path == '/notify/coupon') {
          //Get.toNamed(AppRoutes.Category);
        }
      }, onError: (Object err) {
        print('got err: $err');
      });
    }
  }

  /// 生命周期

  @override
  void onInit() {
    super.onInit();
    handleInitialUri();
    handleIncomingLinks();
    // 准备一些静态数据
    tabTitles = ['倒数时刻', '日历', '设置'];
    bottomTabs = <BottomNavigationBarItem>[
      const BottomNavigationBarItem(
        icon: Icon(
          CupertinoIcons.gift,
          color: AppColors.tabBarElement,
          size: 28,
        ),
        activeIcon: Icon(
          CupertinoIcons.gift,
          color: AppColors.secondaryElementText,
          size: 28,
        ),
        backgroundColor: AppColors.primaryBackground,
      ),
      // const BottomNavigationBarItem(
      //   icon: Icon(
      //     CupertinoIcons.add_circled_solid,
      //     color: Color.fromARGB(255, 41, 103, 200),
      //     size: 36,
      //   ),
      //   activeIcon: Icon(
      //     CupertinoIcons.add_circled_solid,
      //     color: Color.fromARGB(255, 41, 103, 255),
      //     size: 36,
      //   ),
      //   backgroundColor: AppColors.primaryBackground,
      // ),
      const BottomNavigationBarItem(
        icon: Icon(
          CupertinoIcons.calendar,
          color: AppColors.tabBarElement,
          size: 28,
        ),
        activeIcon: Icon(
          CupertinoIcons.calendar,
          color: AppColors.secondaryElementText,
          size: 28,
        ),
        backgroundColor: AppColors.primaryBackground,
      ),
    ];
    pageController = PageController(initialPage: state.page);
  }

  @override
  void dispose() {
    uriSub?.cancel();
    pageController.dispose();
    super.dispose();
  }
}
