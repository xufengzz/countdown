import 'package:countdown/common/values/values.dart';
import 'package:countdown/common/widgets/widgets.dart';
import 'package:countdown/page/calendar/index.dart';
import 'package:countdown/page/list/index.dart';
import 'package:countdown/page/settings/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'index.dart';

class ApplicationPage extends GetView<ApplicationController> {
  const ApplicationPage({super.key});

  // // 顶部导航
  // AppBar _buildAppBar() {
  //   return transparentAppBar(
  //       title: Obx(() => Text(
  //             controller.tabTitles[controller.state.page],
  //             style: TextStyle(
  //               color: AppColors.primaryText,
  //               fontFamily: 'Montserrat',
  //               fontSize: 18.sp,
  //               fontWeight: FontWeight.w600,
  //             ),
  //           )),
  //       actions: <Widget>[
  //         IconButton(
  //           icon: const Icon(
  //             CupertinoIcons.add,
  //             color: AppColors.primaryText,
  //           ),
  //           onPressed: () {},
  //         )
  //       ]);
  // }

  // 内容页
  Widget _buildPageView() {
    return PageView(
      physics: const NeverScrollableScrollPhysics(),
      controller: controller.pageController,
      onPageChanged: controller.handlePageChanged,
      children: const <Widget>[
        ListPage(),
        CalendarPage(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildPageView(),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  // 底部导航
  Widget _buildBottomNavigationBar() {
    return Obx(() => CupertinoTabBar(
          border: const Border.fromBorderSide(BorderSide.none),
          backgroundColor: AppColors.primaryBackground,
          items: controller.bottomTabs,
          currentIndex: controller.state.page,
          onTap: controller.handleNavBarTap,
        ));
  }
}
