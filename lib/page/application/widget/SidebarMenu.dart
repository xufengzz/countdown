import 'package:bruno/bruno.dart';
import 'package:countdown/AdsConfig.dart';
import 'package:countdown/common/routers/names.dart';
import 'package:countdown/common/store/config.dart';
import 'package:countdown/common/values/values.dart';
import 'package:countdown/common/widgets/lottie.dart';
import 'package:countdown/common/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';

//初始化右边的侧边栏
//初始化右边的侧边栏

Widget buildSideMenu(bool isOpened, sideMenuKey, Widget child) {
  toggleMenu([bool end = false]) {
    final state = sideMenuKey.currentState!;
    if (state.isOpened) {
      state.closeSideMenu();
    } else {
      state.openSideMenu();
    }
  }

  return SideMenu(
      closeIcon: const Icon(
        Icons.close,
        color: Colors.transparent,
      ),
      maxMenuWidth: 230.w,
      key: sideMenuKey,
      // end side menu
      background: AppColors.primaryElement.withOpacity(0.2),
      type: SideMenuType.slideNRotate,
      //菜单内容padding
      menu: Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8),
          child: buildMenu()),
      //首页内容
      child: IgnorePointer(
        ignoring: false,
        child: Scaffold(
          backgroundColor: AppColors.primaryBackground,
          body: child,
        ),
      ));
}

Widget buildMenu() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Padding(
        padding: const EdgeInsets.only(left: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundColor: Colors.white,
                  radius: 33.0.w,
                  child: Image.asset(
                    'assets/images/boy_avatar.png',
                    fit: BoxFit.fill,
                    width: 100.w,
                    height: 100.h,
                  ),
                ),
                loadingBlackCat(),
              ],
            ),
            SizedBox(height: 16.0),
            Text(
              "Hello, 游客",
              style: TextStyle(
                color: Colors.white,
                fontFamily: "Avenir",
                fontWeight: FontWeight.w400,
                fontSize: 18.sp,
              ),
            ),
            SizedBox(height: 20.0),
          ],
        ),
      ),
      ListTile(
        onTap: () {},
        leading: const Icon(Icons.mail, size: 20.0, color: Colors.white),
        title: Text("信息",
            style: TextStyle(
              color: Colors.white,
              fontFamily: "Avenir",
              fontWeight: FontWeight.w400,
              fontSize: 16.sp,
            )),
        textColor: Colors.white,
        dense: true,

        // padding: EdgeInsets.zero,
      ),
      // ListTile(
      //   onTap: () {},
      //   leading: const Icon(Icons.star_border, size: 20.0, color: Colors.white),
      //   title: Text(
      //     "归档数据",
      //     style: TextStyle(
      //       color: Colors.white,
      //       fontFamily: "Avenir",
      //       fontWeight: FontWeight.w400,
      //       fontSize: 16.sp,
      //     ),
      //   ),
      //   textColor: Colors.white,
      //   dense: true,
      //
      //   // padding: EdgeInsets.zero,
      // ),
      ListTile(
        onTap: () {
          Get.toNamed(AppRoutes.settings);
        },
        leading: const Icon(Icons.settings, size: 20.0, color: Colors.white),
        title: Text(
          "设置",
          style: TextStyle(
            color: Colors.white,
            fontFamily: "Avenir",
            fontWeight: FontWeight.w400,
            fontSize: 16.sp,
          ),
        ),
        textColor: Colors.white,
        dense: true,
      ),
      const Spacer(),
      ListTile(
        onTap: () {},
        title: Text(
          "版本号${ConfigStore.to.version} (${ConfigStore.to.buildNumber})",
          style: TextStyle(
            color: Colors.white,
            fontFamily: "Avenir",
            fontWeight: FontWeight.w400,
            fontSize: 14.sp,
          ),
        ),
        textColor: Colors.white,
        dense: true,
      ),
    ],
  );
}
