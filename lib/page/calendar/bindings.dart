import 'package:get/get.dart';

import 'controller.dart';

class CalendarBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CalendarViewController>(() => CalendarViewController());
  }
}
