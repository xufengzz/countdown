import 'package:bruno/bruno.dart';
import 'package:countdown/common/values/values.dart';
import 'package:countdown/page/calendar/widget/CalendarView.dart';
import 'package:countdown/page/list/controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';
import '../settings/index.dart';
import 'index.dart';

class CalendarPage extends GetView<CalendarViewController> {
  const CalendarPage({super.key});

  @override
  Widget build(BuildContext context) {
    ListController listController = Get.find();

    BrnAppBar _buildAppBar() {
      return BrnAppBar(
        backgroundColor: AppColors.primaryBackground,
        //默认显示返回按钮
        automaticallyImplyLeading: false,
        //外部主题。状态栏颜色
        brightness: Brightness.light,
        title: Text(
          "日历查看",
          style: TextStyle(
            color: AppColors.primaryText,
            fontFamily: 'Montserrat',
            fontSize: 18.sp,
            fontWeight: FontWeight.w600,
          ),
        ),
      );
    }

    return Scaffold(
      appBar: _buildAppBar(),
      backgroundColor: AppColors.primaryBackground,
      body: Center(
        child: GetBuilder<ListController>(
          init: listController,
          builder: (controller) {
            return CalendarView();
          },
        ),
      ),
    );
  }
}
