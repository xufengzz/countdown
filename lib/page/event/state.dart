import 'package:countdown/common/utils/utils.dart';
import 'package:get/get.dart';
import 'package:lunar/calendar/Lunar.dart';

import '../../common/model/model.dart';
import '../list/widget/PopupMenu.dart';

class EventState {
  var secondaryButtonList = [
    ButtonPanelConfig(name: '倒数日', emoji: "⏳", value: 1),
    ButtonPanelConfig(name: '纪念日', emoji: "⏰", value: 2),
    ButtonPanelConfig(name: '生日', emoji: "🎂", value: 3),
    ButtonPanelConfig(name: '年龄', emoji: "🙇‍", value: 4),
  ];

  //type翻译
  var type = {"1": "倒数日", "2": "纪念日", "3": "生日", "4": "年龄"};

  var lable = {"1": "事件标题", "2": "事件标题", "3": "谁的生日", "4": "谁的年龄"};
  var dateLable = {"1": "日期", "2": "日期", "3": "出生日期", "4": "出生日期"};

  // 当前选中菜单面板
  late final ButtonPanelConfig _buttonPanelConfig = ButtonPanelConfig();

  ButtonPanelConfig get buttonPanelConfig => _buttonPanelConfig;

  set buttonPanelConfig(ButtonPanelConfig value) {
    _buttonPanelConfig.name = value.name;
    _buttonPanelConfig.value = value.value;
    _buttonPanelConfig.emoji = value.emoji;
  }

  //事件对象  下面的字段后面转移到这里 或者单独写个类。太多了乱
  final _eventRecords = Rx<EventRecord?>(null);
  //处理一下过去天数
  set eventRecords(value) => _eventRecords.value = value;

  get eventRecords => _eventRecords.value;

  //用户临时选中的emoji
  late final _emojiTemp = "".obs;

  set emojiTemp(value) => _emojiTemp.value = value;

  get emojiTemp => _emojiTemp.value;

  //打开键盘标识
  final Rx<bool> _openKeyword = false.obs;

  set openKeyword(value) => _openKeyword.value = value;

  bool get openKeyword => _openKeyword.value;

  //标题
  final _title = "".obs;

  set title(value) => _title.value = value;

  get title => _title.value;

  //日期
  final _date = DateTime.now().obs;

  set date(value) {
    _date.value = value;
    //算距离多久
    setDayBetween(value);
    //算年龄
    setAge(value);
  }

  get date => _date.value;

  //是否阴历
  final _lunar = false.obs;

  set lunar(value) {
    _lunar.value = value;
  }

  get lunar => _lunar.value;

  //距离多少日
  final _dayBetween = 0.obs;

  setDayBetween(value) {
    _dayBetween.value = daysBetween(value!, DateTime.now());
  }

  get dayBetween => _dayBetween.value;

  //多少岁
  final _age = 0.obs;

  setAge(value) {
    _age.value = yearBetween(value!, DateTime.now());
  }

  get age => _age.value;

  //模板
  final _cardTitle = ''.obs;

  set cardTitle(value) {
    _cardTitle.value = value;
  }

  get cardTitle {
    //处理卡片标题
    switch (buttonPanelConfig.value) {
      case 1:
        return _cardTitle.value =
            _cardTitle.value == '' ? lable[1.toString()]! : _cardTitle.value;
      case 2:
        return _cardTitle.value =
            _cardTitle.value == '' ? lable[2.toString()]! : _cardTitle.value;
      case 3:
        return _cardTitle.value =
            "${title == '' ? '我的' : title + "的"}${age == 0 ? '' : "$age岁"}生日";
      case 4:
        return _cardTitle.value =
            _cardTitle.value == '' ? lable[4.toString()]! : _cardTitle.value;
    }
  }

  //卡片副标题
  final _cardSubTitle = ''.obs;

  // setCardSubTitle() {
  //   //处理卡片标题
  //   switch (buttonPanelConfig.value) {
  //     case 1:
  //       return date;
  //     case 2:
  //       return date;
  //     case 3:
  //       return formatDateDay(date, lunar: lunar);
  //     case 4:
  //       return date;
  //   }
  // }

  get cardSubTitle {
    //处理卡片标题
    switch (buttonPanelConfig.value) {
      case 1:
        return formatDateDay(date, lunar: lunar);
      case 2:
        return formatDateDay(date, lunar: lunar);
      case 3:
        return formatDateDay(date, lunar: lunar);
      case 4:
        return formatDateDay(date, lunar: lunar);
    }
  }

  //是否编辑模式回显数据
  final _edit = false.obs;
  set edit(value) => _edit.value = value;
  get edit => _edit.value;
  //回显数据
  setInit(EventRecord obj) {
    title = obj.title;
    cardTitle = obj.cardTitle;
    date = obj.date;
  }
}
