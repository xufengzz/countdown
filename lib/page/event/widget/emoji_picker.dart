import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter/material.dart';

//emoji键盘选择器
void showEmojiPicker(context, bool emojiShowing,
    {required Function(Emoji) onTap}) {
  showDialog(
    barrierDismissible: true,
    context: context,
    builder: (context) {
// 用Scaffold返回显示的内容，能跟随主题
      return GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Scaffold(
              backgroundColor: Colors.transparent, // 设置透明背影

              body: Stack(children: <Widget>[
                EmojiPickerWidget(
                  emojiShowing: emojiShowing,
                  onTap: (emoji) {
                    onTap(emoji);
                  },
                )
              ])));
    },
  );
}

/// Example for EmojiPickerFlutter
class EmojiPickerWidget extends StatefulWidget {
  //打开emoji键盘
  final bool emojiShowing;

  /// 点击回调
  final void Function(Emoji) onTap;

  const EmojiPickerWidget(
      {super.key, required this.emojiShowing, required this.onTap});

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<EmojiPickerWidget> {
  final TextEditingController _controller = TextEditingController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
        left: 0,
        right: 0,
        bottom: 0,
        child: Offstage(
          offstage: !widget.emojiShowing,
          child: SizedBox(
              height: 250,
              child: EmojiPicker(
                onEmojiSelected: (Category? category, Emoji emoji) {
                  widget.onTap!(emoji);
                },
                textEditingController: _controller,
                config: Config(
                  columns: 7,
                  // Issue: https://github.com/flutter/flutter/issues/28894
                  emojiSizeMax: 32 *
                      (foundation.defaultTargetPlatform == TargetPlatform.iOS
                          ? 1.30
                          : 1.0),
                  verticalSpacing: 0,
                  horizontalSpacing: 0,
                  gridPadding: EdgeInsets.zero,
                  initCategory: Category.RECENT,
                  bgColor: const Color(0xFFF2F2F2),
                  indicatorColor: Colors.blue,
                  iconColor: Colors.grey,
                  iconColorSelected: Colors.blue,
                  backspaceColor: Colors.blue,
                  skinToneDialogBgColor: Colors.white,
                  skinToneIndicatorColor: Colors.grey,
                  enableSkinTones: true,
                  showRecentsTab: true,
                  recentsLimit: 28,
                  replaceEmojiOnLimitExceed: false,
                  noRecents: const Text(
                    'No Recents',
                    style: TextStyle(fontSize: 20, color: Colors.black26),
                    textAlign: TextAlign.center,
                  ),
                  loadingIndicator: const SizedBox.shrink(),
                  tabIndicatorAnimDuration: kTabScrollDuration,
                  categoryIcons: const CategoryIcons(),
                  buttonMode: ButtonMode.MATERIAL,
                  checkPlatformCompatibility: true,
                ),
              )),
        ));
  }
}
