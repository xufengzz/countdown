import 'dart:convert';

import 'package:bruno/bruno.dart';
import 'package:countdown/common/style/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter/services.dart' show rootBundle;

///打开用户隐私组件
///创建很长的网页可能会使您的应用崩溃
class WebViewPage extends StatefulWidget {
  final String title;

  //0=协议 1=隐私
  final int type;

  const WebViewPage({
    Key? key,
    required this.title,
    required this.type,
  }) : super(key: key);

  @override
  _WebViewPageState createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  late WebViewController _controller;

  String userAgreement = 'assets/files/服务条款.html';
  String userPrivacy = 'assets/files/用户隐私.html';
  late double _height = 300.h;
  late bool _isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: BrnAppBar(
        backgroundColor: AppColor.scaffoldBackground,
        //默认显示返回按钮
        automaticallyImplyLeading: false,
        title: Text(
          "${widget.title}",
          style: TextStyle(
            fontSize: 16.sp,
            color: Colors.black.withOpacity(0.7),
          ),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
            size: 18.sp,
            color: Colors.black.withOpacity(0.7),
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Stack(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: _height,
            child: WebView(
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                _controller = webViewController;
                _loadhtmlfromassets();
              },
              onPageFinished: (url) async {
                //调用JS得到实际高度
                var type =
                    await _controller.evaluateJavascript("document.compatMode");
                if (type == "CSS1Compat") {
                  _height = double.parse(await _controller.evaluateJavascript(
                      "document.documentElement.scrollHeight;"));
                } else {
                  _height = double.parse(await _controller
                      .evaluateJavascript("document.body.scrollHeight;"));
                }
                setState(() {});
              },
            ),
          ),
          Offstage(
            offstage: _isLoading,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  _loadhtmlfromassets() async {
    String filaments = await rootBundle.loadString(userAgreement);
    switch (widget.type) {
      case 0:
        break;
      case 1:
        filaments = await rootBundle.loadString(userPrivacy);
        break;
    }
    await _controller.loadUrl(Uri.dataFromString(filaments,
            mimeType: 'text/html', encoding: Encoding.getByName('utf-8'))
        .toString());
    //已经加载完成
    setState(() {
      _isLoading = true;
    });
  }
}
