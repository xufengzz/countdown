import 'package:countdown/common/style/color.dart';
import 'package:countdown/common/values/values.dart';
import 'package:countdown/page/frame/welcome/widget/web_view_page.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

//隐私协议
Widget content(BuildContext context) {
  return Padding(
    padding:
        EdgeInsets.only(left: 37.sp, right: 37.sp, top: 4.sp, bottom: 8.sp),
    child: RichText(
      textAlign: TextAlign.center,
      text: TextSpan(children: [
        TextSpan(
          text: '请阅读我们的   ',
          style: TextStyle(fontSize: 16.sp, color: Colors.grey),
        ),
        TextSpan(
            text: '隐私协议',
            style: TextStyle(
              fontSize: 16.sp,
              color: AppColor.primaryBackground,
            ),
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return const WebViewPage(title: '隐私协议', type: 1);
                }));
                // 查看 服务条款
              }),
        TextSpan(
          text: ' 点击“同意并继续”则代表接受 ',
          style: TextStyle(fontSize: 16.sp, color: Colors.grey),
        ),
        TextSpan(
            text: '服务条款',
            style: TextStyle(fontSize: 16.sp, color: AppColors.primaryElement),
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return const WebViewPage(title: '服务条款', type: 0);
                }));
                // 查看 服务条款
              }),
        TextSpan(
          text: ' 。',
          style: TextStyle(fontSize: 14.sp, color: Colors.grey),
        ),
      ]),
    ),
  );
}
