import 'package:countdown/common/utils/utils.dart';
import 'package:get/get.dart';

import '../../common/model/model.dart';

class ListState {
  final _eventRecords = Rx<List<EventRecord>?>(null);

  //处理一下过去天数
  set eventRecords(var value) {
    if (value != null) {
      for (var o in value) {
        o.dayBetween = daysBetween(o.date!, DateTime.now());
      }
    }
    _eventRecords.value = value;
  }

  get eventRecords => _eventRecords.value;
}
