import 'package:countdown/common/style/color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'Model.dart';
import 'Popup.dart';

/// 次按钮的配置类
class ButtonPanelConfig {
  /// 次按钮的名称
  String? name;
  int? value;
  String? emoji;

  ButtonPanelConfig({
    this.name = "倒数日",
    this.value = 1,
    this.emoji,
  });
}

///打开菜单
class PopupMenu extends StatefulWidget {
  const PopupMenu(
      {super.key,
      required this.icon,
      required this.secondaryButtonList,
      required this.secondaryButtonOnTap});

  //图标
  final Icon icon;

  //子菜单
  final List<ButtonPanelConfig> secondaryButtonList;

  /// 次按钮的点击回调
  final void Function(ButtonPanelConfig)? secondaryButtonOnTap;

  @override
  _PopupMenuState createState() => _PopupMenuState();
}

class _PopupMenuState extends State<PopupMenu> {
  ///给获取详细信息的widget设置一个key
  GlobalKey iconkey = GlobalKey();

  ///获取位置，给后续弹窗设置位置
  late Offset iconOffset;

  ///获取size 后续计算弹出位置
  late Size iconSize;

  ///接受弹窗类构造成功传递来的关闭参数
  late Function closeModel;

  @override
  Widget build(BuildContext context) {
    ///等待widget初始化完成
    WidgetsBinding.instance.addPostFrameCallback((duration) {
      ///通过key获取到widget的位置
      RenderBox box = iconkey.currentContext!.findRenderObject()! as RenderBox;

      ///获取widget的高宽
      iconSize = box.size;

      ///获取位置
      iconOffset = box.localToGlobal(Offset.zero);
    });

    return IconButton(
      key: iconkey,
      icon: widget.icon,
      onPressed: () {
        showModel(context);
      },
    );
  }

  ///播放动画
  void showModel(BuildContext context) {
    /// 设置传入弹窗的高宽
    double _width = 130.w;
    double _height = 230.h;

    Navigator.push(
      context,
      Popup(
        child: Model(
          left: iconOffset.dx - _width + iconSize.width / 1.0,
          top: iconOffset.dy + iconSize.height / 1.3.w,
          offset: Offset(_width / 2.w, -_height / 2.w),
          child: SizedBox(
            width: _width,
            height: _height,
            child: buildMenu(),
          ),
          fun: (close) {
            closeModel = close;
          },
        ),
      ),
    );
  }

  ///构造传入的widget
  Widget buildMenu() {
    return Container(
      height: 160.w,
      width: 230.h,
      child: Stack(
        children: [
          Positioned(
            right: 4.sp,
            top: 18.sp,
            child: Container(
              width: 18.w,
              height: 18.h,
              transform: Matrix4.rotationZ(45 * 3.14 / 180),
              decoration: BoxDecoration(
                color: const Color.fromRGBO(46, 53, 61, 1),
                borderRadius: BorderRadius.circular(5),
              ),
            ),
          ),

          ///菜单内容
          Positioned(
            bottom: 0,
            child: Container(
              padding: EdgeInsets.only(
                top: 20.sp,
                bottom: 20.sp,
                left: 10.sp,
                right: 10.sp,
              ),
              width: 130.w,
              height: 200.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.sp),
                color: const Color.fromRGBO(46, 53, 61, 1),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: widget.secondaryButtonList
                    .map<Widget>((e) => InkWell(
                          child: Container(
                            width: double.infinity,
                            alignment: Alignment.bottomRight,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  '${e.name}',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.sp,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 12.sp),
                                  child: Text(
                                    '${e.emoji}',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.sp,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () async {
                            await closeModel();
                            widget.secondaryButtonOnTap!(e);
                          },
                        ))
                    .toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
