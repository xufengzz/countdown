//抽象首页的组件
import 'package:countdown/common/model/model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../common/utils/utils.dart';

Widget buildListTitle(EventRecord eventRecord) {
  return Container(
    height: 70.h,
    alignment: Alignment.center,
    decoration: const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(14.0)),
    ),
    child: ListTile(
      //缩小布局
      //dense: true,
      //紧凑 只能在-4 和4之间
      visualDensity: const VisualDensity(horizontal: -4),
      leading: Container(
          padding: const EdgeInsets.only(left: 10, top: 8, bottom: 4),
          child: RichText(
              text: TextSpan(children: <TextSpan>[
            TextSpan(
                text: "${eventRecord.emoji}",
                style: const TextStyle(
                  fontSize: 32,
                )),
          ]))),
      // 标题
      title: Text(
        '${eventRecord.cardTitle}',
        style: TextStyle(
            fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
      ),
      subtitle: Text(
        '${eventRecord.cardSubTitle}',
        style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.bold,
            color: Colors.black.withOpacity(0.5)),
      ),
      trailing: Container(
        child: eventRecord.type == 3
            ? Text(
                "${formatBirthday(eventRecord.date!, lunar: eventRecord.lunar!)} 天")
            : eventRecord.type == 4
                ? Text(getAgeByBirthday(eventRecord.date!))
                : Text(
                    "${format(eventRecord.dayBetween, type: eventRecord.type!)}"),
      ),
    ),
  );
}
