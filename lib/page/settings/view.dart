import 'package:bruno/bruno.dart';
import 'package:countdown/common/values/values.dart';
import 'package:countdown/page/frame/welcome/widget/web_view_page.dart';
import 'package:countdown/page/settings/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

import '../calendar/index.dart';

class SettingsPage extends GetView<SettingsController> {
  const SettingsPage({super.key});

  PreferredSizeWidget _buildAppBar() {
    return BrnAppBar(
        backgroundColor: AppColors.primaryBackground,
        //默认显示返回按钮
        automaticallyImplyLeading: false,
        //外部主题。状态栏颜色
        brightness: Brightness.light,
        leadingWidth: 70,
        leading: Container(
          padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          child: Text(
            "设置",
            style: TextStyle(
              color: AppColors.primaryText,
              fontFamily: 'Montserrat',
              fontSize: 22.sp,
              fontWeight: FontWeight.w600,
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primaryBackground,
      appBar: _buildAppBar(),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
        child: Center(
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: Radii.k6pxRadius,
                ),
                child: Row(
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return const WebViewPage(title: '隐私协议', type: 1);
                          }));
                        },
                        child: Text(
                          "查看隐私策略",
                          style:
                              TextStyle(color: Colors.black, fontSize: 16.sp),
                        ))
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: Radii.k6pxRadius,
                ),
                child: Row(
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return const WebViewPage(title: '查看服务条款', type: 0);
                          }));
                        },
                        child: Text(
                          "查看服务条款",
                          style:
                              TextStyle(color: Colors.black, fontSize: 16.sp),
                        ))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
